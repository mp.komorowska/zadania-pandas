conditions = [
    (df['doswiadczenie'] <= 3),
    (df['doswiadczenie'] >= 4) & (df['doswiadczenie'] <= 8),
    (df['doswiadczenie'] >= 9) & (df['doswiadczenie'] <= 12),
    (df['doswiadczenie'] >= 13)
]

values = ['Junior', 'Mid', 'Senior', 'Ekspert']

df['staz'] = np.select(conditions,values)

df.staz
